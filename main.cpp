#include <iostream>

using namespace std;

int main()
{
    int hrs_1,mins_1,hrs,mins,duratahrs,duratamins;
    cout << "La ce ora a plecat trenul din Sibiu ? " << endl;
    cout << "Ora : "; cin >> hrs;
    while(hrs>23 || hrs<00){
        cout << "Ora nu a fost introdusa corespunzator!" << endl;
        cout << "La ce ora a plecat trenul din Sibiu ? " << endl;
    cout << "Ora : "; cin >> hrs;
    }
    cout << "Minutul : "; cin >> mins;
    while(mins>59 || mins<00){
        cout << "Minutul nu a fost introdus corespunzator!" << endl;
        cout << "Minutul : "; cin >> mins;
    }
    cout << "La ce ora a ajuns trenul in Bucuresti ? " << endl;
    cout << "Ora : "; cin >> hrs_1;
    while(hrs_1>23 || hrs_1<00){
        cout << "Ora nu a fost introdusa corespunzator!" << endl;
        cout << "La ce ora a ajuns trenul in Bucuresti ? " << endl;
        cout << "Ora : "; cin >> hrs_1;
    }
    cout << "Minutul : "; cin >> mins_1;
    while(mins_1>59 || mins_1<00){
        cout << "Minutul nu a fost introdus corespunzator!" << endl;
        cout << "Minutul : "; cin >> mins_1;
    }
    cout << "Ora plecarii: ora " << hrs << " si " <<  mins << " minute" << endl;
    cout << "Ora sosirii: ora " << hrs_1 << " si " << mins_1 << " minute" << endl;
    if(hrs <= hrs_1){
        duratahrs = hrs_1 - hrs;
    }
    else if(hrs > hrs_1 && mins <= mins_1){
        duratahrs = 24 - (hrs - hrs_1);
    }
    else if(hrs > hrs_1 && mins > mins_1){
        duratahrs = 23 - (hrs - hrs_1);
    }
    if(mins <= mins_1){
        duratamins = mins_1 - mins;
    }
    else if(mins > mins_1){
        duratamins = 60 - (mins - mins_1);
    }
    cout << "Durata drumului : " << duratahrs << " ore si " << duratamins << " minute" << endl;
    return 0;
}
